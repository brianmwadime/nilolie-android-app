package com.mwadey.nilolie.apis;

import java.util.List;

import com.google.gson.Gson;
import com.littlefluffytoys.littlefluffylocationlibrary.LocationInfo;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mwadey.nilolie.models.HospitalResponse;
import com.mwadey.nilolie.models.HospitalResult;

/**
 * Wrapper for opendata REST API.
 */
public class OpendataApi {

	private static final String API_BASE_URL = "http://afrigeeks.com/opendata/api/1"; 
	
	private AsyncHttpClient httpClient;
	
	public OpendataApi() {
		httpClient = new AsyncHttpClient();
		httpClient.addHeader("Accept", "application/json");
		httpClient.setTimeout(30000);
	}

	/**
	 * Get hospitals.
	 * 
	 * @param listener
	 * @param 
	 */
	public void getHospitals(LocationInfo geoPoint, final ResultsListener listener) {
		RequestParams params = new RequestParams();
		
		if (geoPoint != null) {
			params.put("lat", Double.toString(geoPoint.lastLat));
			params.put("lon", Double.toString(geoPoint.lastLong));
		}else{
			params.put("lat", "-1.2838557" );
			params.put("lon","36.8826246");
		}
		//params.put("lon", Double.toString(geoPoint.getLongitude()));
		httpClient.get(API_BASE_URL + "/hospitals/locate/nearby",params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String result) {
				Gson gson = new Gson();
		        
		        HospitalResponse response = gson.fromJson(result, HospitalResponse.class);
		        
		        List<HospitalResult> hospitals = response.data;
		        //Collection<HospitalResult> hospitals = response.data;
				listener.onSuccess(hospitals);
			}

			@Override
			public void onFailure(Throwable t, String message) {
				listener.onFailure();
			}
		});
	}
	
	/**
	 * Get specific hospital by it's ID.
	 * 
	 * @param id
	 * @param listener
	 */
	public void getHospital(int id, final HospitalListener listener) {
		httpClient.get(API_BASE_URL + "/hospitals/" + id, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String result) {
				listener.onSuccess(null);
			}
			
			@Override
			public void onFailure(Throwable t, String message) {
				listener.onFailure();
			}
		});
	}
	
	
	public interface ResultsListener {
		public void onSuccess(List<HospitalResult> results);
		public void onFailure();
	}
	
	public interface HospitalListener {
		public void onSuccess(HospitalResult hospital);
		public void onFailure();
	}
	

}
