package com.mwadey.nilolie.adapters;

import java.util.List;
import org.apache.commons.lang3.*;
import org.apache.commons.lang3.text.WordUtils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.mwadey.nilolie.R;
import com.mwadey.nilolie.models.HospitalResult;

public class HospitalAdapter extends ArrayAdapter<HospitalResult> {

	private List<HospitalResult> items;
	private LayoutInflater inflater;
		
	public HospitalAdapter(Context context, List<HospitalResult> items) {
		super(context, 0, items);
		
		this.items = items;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
	}
	
	/**
	 * Return an object in the data set.
	 */
	public HospitalResult getItem(int position) {
		return this.items.get(position);
	}

	/**
	 * Return the position provided.
	 */
	public long getItemId(int position) {
		return position;
	}
	
	public View getView(final int position, View convertView, ViewGroup parent) {
		 HospitalResult hospital = items.get(position);
        
		ViewHolder viewHolder = null;
		
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.list_item, parent, false);
			
			viewHolder = new ViewHolder();
			viewHolder.hopitalName = (TextView) convertView.findViewById(R.id.textView_name);
			viewHolder.hospitalLocation = (TextView) convertView.findViewById(R.id.textView_loc);
			viewHolder.hospitalType = (TextView) convertView.findViewById(R.id.textView_facValue);
			viewHolder.hospitalDistance = (TextView) convertView.findViewById(R.id.textView_distance);
			
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
			
		}
		
		// Required because after the bitmap is fetched from network, row should
		// refresh it's imageView only if it WASN'T recycled (because of ListView architecture)
		viewHolder.position = position;
		
		//String hospitalName = hospital.name;
		//if(hospitalName != null && hospitalName.length() > 0) {
			viewHolder.hopitalName.setText(WordUtils.capitalizeFully(hospital.name));
			viewHolder.hospitalLocation.setText(hospital.location);
			viewHolder.hospitalType.setText(facultyType(hospital.faculty));
			viewHolder.hospitalDistance.setText(distance(hospital.distance));
			//viewHolder.hopitalName.setVisibility(View.VISIBLE);
		//} else {
		//	viewHolder.hopitalName.setVisibility(View.GONE);
		//}
		
        return convertView;
    }
	
	public static class ViewHolder {
		public TextView hopitalName;
		public TextView hospitalLocation;
		public TextView hospitalType;
		public TextView hospitalDistance;
		public int position;
	}
	
	@Override
	public int getCount() {
		return (items != null) ? items.size() : 0;
	}
	
	public String distance(String distance){
			if (distance instanceof String && distance.contains(".")){
				String[] dist = distance.split("\\.");
				if (Integer.parseInt(dist[0]) > -1){
					return dist[0] + "." + dist[1].substring(0,1) + "km";
				}else{
					return dist[1].substring(0,3) + "m";
				}
			}else{
				return "unknown";
			}
	}
	
	
	private String facultyType(int type){
		
		switch (type) {
		case 1:
			
			return "District Hospital";
		case 2:
			
			return "Provincial Hospital";
		case 3:
			
			return "Health Centre";
		
		case 4:
			
			return "Dispensary";
			
		case 5:
			
			return "Private Hospital";
		case 6:
			
			return "Private Clinic";
		case 7:
			
			return "Nursing / Maternity Home";
		case 8:
			
			return "Special Clinic";
		case 9:
			
			return "Institutional Facility";
		default:
			
			return "Not Categorized";
		}
		
		
	}
	
}
