package com.mwadey.nilolie;

import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import com.mwadey.nilolie.base.BaseActivity;
import com.mwadey.nilolie.models.HospitalResult;
import com.mwadey.nilolie.models.LatLonGeoPoint;
import com.mwadey.nilolie.overlays.LocationOverlay;


public class MapsActivity extends BaseActivity {

	/**
	 * {@link Location} provided from calling activity.
	 */
	public static final String EXTRA_LOCATION = "hospital";
	private LatLng HospitalLocation;
	private HospitalResult hospital; 
	
	private GoogleMap mMap;
	private MapFragment mapFragment;
	private TextView mapTitle;
	
	@Override
	public void initUI() {
		mapTitle = (TextView) findViewById(R.id.textView_Title);
		mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapView);
		mMap = mapFragment.getMap();
		
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_maps);
		
		Bundle b = getIntent().getExtras();
		if(b != null) {
			hospital = b.getParcelable(EXTRA_LOCATION);
			Toast.makeText(MapsActivity.this, hospital.toString(), Toast.LENGTH_LONG).show();
			
			initMapTitleView();
			initMapView();
			
		}
		
	}
	
	/**
	 * Set the {@link TextView} to hospital Name.
	 */
	private void initMapTitleView() {
		if(hospital.getName() != null) {
			mapTitle.setText(hospital.getName());
		}
	}
	
	private void initMapView(){
		HospitalLocation = new LatLng((Double) hospital.getLat(), (Double) hospital.getLon());
		//mMap.getUiSettings().setZoomControlsEnabled(false);
		mMap.setMyLocationEnabled(true);
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(HospitalLocation, 17));
        if (mMap!=null){
          mMap.addMarker(new MarkerOptions()
              .position(HospitalLocation)
              .title(hospital.getName())
              .snippet(hospital.getFaculty())
              .icon(BitmapDescriptorFactory
                  .fromResource(R.drawable.map_marker)));
        }
        
	}


	protected boolean isRouteDisplayed() {
		return false;
	}

}
