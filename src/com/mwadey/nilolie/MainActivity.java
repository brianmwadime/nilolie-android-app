package com.mwadey.nilolie;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.littlefluffytoys.littlefluffylocationlibrary.LocationInfo;
import com.littlefluffytoys.littlefluffylocationlibrary.LocationLibrary;
import com.mwadey.nilolie.adapters.HospitalAdapter;
import com.mwadey.nilolie.base.BaseActivity;
import com.mwadey.nilolie.models.HospitalResult;
import com.mwadey.nilolie.models.HospitalResponse;
import com.mwadey.nilolie.apis.OpendataApi.ResultsListener;

import static com.mwadey.nilolie.utils.LogUtils.makeLogTag;

public class MainActivity extends BaseActivity {
	
	
	private static final String TAG = makeLogTag(MainActivity.class);
	protected LocationInfo appLocation;
	/**
     * Progress bar
     */
    protected ProgressBar progressBar;
    /**
     * Empty view
     */
    protected TextView emptyView;
    /**
     * Is the list currently shown?
     */
    protected boolean listShown;
	/**
	 * {@link Location} provided from calling activity.
	 */
	private List<HospitalResult> listItems = new ArrayList<HospitalResult>();
	private ArrayAdapter<HospitalResult> adapter;
	
	private ListView listView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		if (isLocationProviderAvailable()) {
			LocationLibrary.forceLocationUpdate(this);
			LocationInfo latestInfo = new LocationInfo(getBaseContext());
			Toast.makeText(MainActivity.this, latestInfo.toString(), Toast.LENGTH_LONG).show();
		}
		
		adapter = new HospitalAdapter(this, listItems);
		initListView();
		
		// Fetch new images if necessary
		if(listItems.size() == 0) {
			appLocation = new LocationInfo(getBaseContext());
			//final ProgressDialog dialog = ProgressDialog.show(this, null, getString(R.string.loading_hospitals), true, false);
			progressBar = (ProgressBar) findViewById(R.id.pb_loading);
			fetchHospitals(appLocation,new FetchHospitalsListener() {
				
				@Override
				public void onSuccess() {
					//dialog.cancel();
					progressBar.setVisibility(View.GONE);
					listView.setVisibility(View.VISIBLE);
				}
				
				@Override
				public void onFailure() {
					progressBar.setVisibility(View.GONE);
					emptyView.setVisibility(View.VISIBLE);
				}
			});
		}
	}

	@Override
	public void initUI() {
		//progressBar = (ProgressBar) findViewById(R.id.pb_loading);

        emptyView = (TextView) findViewById(android.R.id.empty);
		listView = (ListView) findViewById(R.id.listView_hospitals);
		
	}
	
	/**
	 * Init the {@link ListView} adapter, labels and {@link OnClickListener} listeners.
	 */
	private void initListView() {
		listView.setAdapter(adapter);
		
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				//position--; // Possible bug due PullToRefresh? [header counts as first row]
				//Toast.makeText(MainActivity.this, listItems.get(position) +" Item Clicked", Toast.LENGTH_LONG).show();
				Intent i = new Intent(MainActivity.this, HospitalActivity.class);
				i.putExtra(HospitalActivity.EXTRA_HOSPITAL, listItems.get(position));
				startActivity(i);
			}
		});
	}
	
	/**
	 * Retrieve hospitals  from REST API.
	 * 
	 * @param listeners
	 */
	private void fetchHospitals(LocationInfo locationData, final FetchHospitalsListener ... listeners) {
		getApi().getHospitals(locationData, new ResultsListener() {
			
			@Override
			public void onSuccess(List<HospitalResult> hospitals) {
				Toast.makeText(MainActivity.this, "done loading items", Toast.LENGTH_LONG).show();
				List<HospitalResult> hospitalsListCreated = new ArrayList<HospitalResult>();
				
				List<HospitalResult> hospitalList = new ArrayList<HospitalResult>(hospitals);
				//Collections.reverse(hospitalList);
				
				
				for(HospitalResult hospital : hospitalList) {
					listItems.add(0, hospital);
				}
				
				adapter.notifyDataSetChanged();
				
				
				for(FetchHospitalsListener listener : listeners) {
					listener.onSuccess();
				}
			}
			
			@Override
			public void onFailure() {
				//listView.onRefreshComplete();
				Toast.makeText(MainActivity.this, "failed to items", Toast.LENGTH_LONG).show();
				for(FetchHospitalsListener listener : listeners) {
					listener.onFailure();
				}
			}
		});
	}
	
	private interface FetchHospitalsListener {
		public void onSuccess();
		public void onFailure();
	}
	
	/**
	 * @return	<code>true</code> if either gps or network provided are available.
	 */
	private boolean isLocationProviderAvailable() {
		LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        
        return gps || network;
	}
	
	@Override
	  public boolean onCreateOptionsMenu(Menu menu) {
	    getMenuInflater().inflate(R.menu.main, menu);
	    return true;
	  }
	
public boolean onOptionsItemSelected(MenuItem item) {
        
        switch (item.getItemId()) {
	        case R.id.menu_refresh:
	        	//refresh();
	            return true;
	        default:
	            return false;
        }
    }

}
