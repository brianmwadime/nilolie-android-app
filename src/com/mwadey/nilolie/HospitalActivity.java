package com.mwadey.nilolie;

import org.apache.commons.lang3.text.WordUtils;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mwadey.nilolie.base.BaseActivity;
import com.mwadey.nilolie.models.HospitalResult;

public class HospitalActivity extends BaseActivity {
	
	
	/**
	 * Hospital retrieved from calling activity.
	 */
	public static final String EXTRA_HOSPITAL = "hospital";
	
	private LatLng HospitalLocation;
	private HospitalResult hospital; 
	private ImageButton map;
	private ImageView separator;
	
	private RelativeLayout contentLayout;
	private TextView name;
	private TextView location;
	private TextView faculty;
	private TextView agency;
	private GoogleMap mMap;
	private MapFragment mapFragment;
	
	public void initUI() {
		map = (ImageButton) findViewById(R.id.imageButton_map);
		separator = (ImageView) findViewById(R.id.separator);
		name = (TextView) findViewById(R.id.textView_name);
		location = (TextView) findViewById(R.id.textView_location);
		faculty = (TextView) findViewById(R.id.textView_faculty);
		agency = (TextView) findViewById(R.id.textView_agency);
		mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map_frag);
		mMap = mapFragment.getMap();
		contentLayout = (RelativeLayout) findViewById(R.id.relativeLayout_content);
		
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hospital);
		
		Bundle b = getIntent().getExtras();
		if(b != null) {
			hospital = b.getParcelable(EXTRA_HOSPITAL);
			Toast.makeText(HospitalActivity.this, hospital.toString(), Toast.LENGTH_LONG).show();
			
			initMapView();
			initNameTextView();
			initLocationTextView();
			initFacultyTextView();
			initagencyTextView();
			initLocationButton();
			
		}
		
	}
	
	private void initMapView(){
		HospitalLocation = new LatLng((Double) hospital.getLat(), (Double) hospital.getLon());
		mMap.getUiSettings().setZoomControlsEnabled(false);
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(HospitalLocation, 17));
        if (mMap!=null){
          Marker hospitalarker = mMap.addMarker(new MarkerOptions()
              .position(HospitalLocation)
              .title(hospital.getName())
              .snippet(hospital.getFaculty())
              .icon(BitmapDescriptorFactory
                  .fromResource(R.drawable.map_marker)));
        }
        
	}
	
	/**
	 * Set the {@link TextView} to hospital Name.
	 */
	private void initNameTextView() {
		if(hospital.getName() != null) {
			contentLayout.setVisibility(View.VISIBLE);
			name.setText(WordUtils.capitalizeFully(hospital.getName()));
		}
	}
	
	private void initLocationTextView() {
		if(hospital.getLocation() != null) {
			contentLayout.setVisibility(View.VISIBLE);
			location.setText(hospital.getLocation());
		}
	}
	
	private void initagencyTextView() {
		if(hospital.getAgency() != null) {
			contentLayout.setVisibility(View.VISIBLE);
			agency.setText(hospital.getAgency());
		}
	}
	
	private void initFacultyTextView() {
		if(hospital.getFaculty() != "unknown") {
			contentLayout.setVisibility(View.VISIBLE);
			faculty.setText(hospital.getFaculty());
		}
	}
	/**
	 * Show icon in ActionBar if there is some location to show.
	 */
	private void initLocationButton() {
		if(hospital.geolocation != null) {
			separator.setVisibility(View.VISIBLE);
			map.setVisibility(View.VISIBLE);
			
			map.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent i = new Intent(HospitalActivity.this, MapsActivity.class);
					i.putExtra(MapsActivity.EXTRA_LOCATION, hospital);
					startActivity(i);
				}
			});
		}
	}

}
