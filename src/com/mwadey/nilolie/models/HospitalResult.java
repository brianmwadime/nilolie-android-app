package com.mwadey.nilolie.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

	
public class HospitalResult implements Parcelable{
	
	public long id;
	
	public String name;
	
	public String HMIS;
	
	public String province;
	
	public String district;
	
	public String division;
	
	public String location;
	
	@SerializedName("sub_location")
	public String subLocation;
	
	@SerializedName("spat_ref_method")
	public String spatRef;
	
	@SerializedName("fac_type")
	public int faculty;
	
	public String agency;
	
	public String geolocation;
	
	public Double lat;
	
	public Double lon;
	
	public String distance;
	
	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getHmis() {
		return HMIS;
	}
	
	public String getProvince() {
		return province;
	}
	
	public String getDistrict() {
		return district;
	}
	
	public String getDivision() {
		return division;
	}
	
	public String getLocation() {
		return location;
	}
	
	public String getSublocation() {
		return subLocation;
	}
	
	public String getSpatref() {
		return spatRef;
	}
	
	public String getFaculty() {
		switch (faculty) {
		case 1:
			
			return "District Hospital";
		case 2:
			
			return "Provincial Hospital";
		case 3:
			
			return "Health Centre";
		
		case 4:
			
			return "Dispensary";
			
		case 5:
			
			return "Private Hospital";
		case 6:
			
			return "Private Clinic";
		case 7:
			
			return "Nursing / Maternity Home";
		case 8:
			
			return "Special Clinic";
		case 9:
			
			return "Institutional Facility";
		default:
			
			return "unknown";
		}
	}
	
	public String getAgency() {
		return agency;
	}
	
	public String getGeolocation() {
		return geolocation;
	}
	
	public Double getLat() {
		return lat;
	}
	
	public Double getLon() {
		return lon;
	}
	
	public String getdistance() {
		return distance;
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public HospitalResult() {
		// TODO Auto-generated constructor stub
	}
	
	public HospitalResult(int id, String name) {
		this.id = id;
		this.name = name;
	}

	

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeLong(id);
		parcel.writeString(name);
		parcel.writeString(HMIS);
		parcel.writeString(province);
		parcel.writeString(district);
		parcel.writeString(division);
		parcel.writeString(location);
		parcel.writeString(subLocation);
		parcel.writeString(spatRef);
		parcel.writeInt(faculty);
		parcel.writeString(agency);
		parcel.writeString(geolocation);
		parcel.writeDouble(lat);
		parcel.writeDouble(lon);
		parcel.writeString(distance);
		
	}
	
	public static final Parcelable.Creator<HospitalResult> CREATOR = new Parcelable.Creator<HospitalResult>() {
		public HospitalResult createFromParcel(Parcel in) {
			HospitalResult route = new HospitalResult();
			route.readFromParcel(in);
			
			return route;
		}

		@Override
		public HospitalResult[] newArray(int size) {
			return new HospitalResult[size];
		}
	};

	protected void readFromParcel(Parcel source) {
		id = source.readLong();
		name = source.readString();
		HMIS = source.readString();
		province = source.readString();
		district = source.readString();
		division = source.readString();
		location = source.readString();
		subLocation = source.readString();
		spatRef = source.readString();
		faculty = source.readInt();
		agency = source.readString();
		geolocation = source.readString();
		lat = source.readDouble();
		lon = source.readDouble();
		distance = source.readString();
		
		
	}
	
	@Override
	public String toString() {
		return "hospitalResult [id=" + id + ", name=" + name
				+ ", location=" + location + ", faculty=" + faculty
				+ ", distance=" + distance + ", lat =" + lat + ", lon=" + lon + "]";
	}

}
