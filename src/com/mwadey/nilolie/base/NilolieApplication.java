package com.mwadey.nilolie.base;

import android.app.Application;
import android.content.Context;

import com.littlefluffytoys.littlefluffylocationlibrary.LocationLibrary;

public class NilolieApplication extends Application {

	
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		LocationLibrary.initialiseLibrary(getBaseContext(), "com.mwadey.nilolie");
	}

	public static NilolieApplication getApplication(Context context) {
		return (NilolieApplication) context.getApplicationContext();
	}
	
	/**
	 * @return	Global {@link DatabaseHelper} helper.
	 */
	
}
