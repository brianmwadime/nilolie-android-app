package com.mwadey.nilolie.base;

import android.app.Activity;
import android.os.Bundle;
import android.os.Debug;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mwadey.nilolie.apis.OpendataApi;
import com.mwadey.nilolie.utils.LogUtils;

public abstract class BaseActivity extends Activity {

	private OpendataApi api;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		api = new OpendataApi();
	}
	
	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
		
		initUI();
		
		ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content).getRootView();
	}

	/**
	 * @return	Global {@link ImageWallApi} to use for REST API access.
	 */
	public OpendataApi getApi() {
		return api;
	}
	
	/**
	 * <p>Initialize your layout views. For example</p>
	 * <code>TextView header = (TextView) findViewById(R.id.textView_header);</code>
	 */
	public abstract void initUI();
	
	public void showToast(String message) {
	      	
	    	if(message == null || message.length() == 0) return;
	    	
	    	try{
		    	Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
		    	toast.setGravity(Gravity.CENTER, 0, 0);
		    	toast.show();
	    	}catch(Exception e){
	    		//LOGV("Toaster","Toast Error :" + e);
	    	}
	    }
		
	}
